package com.example.weeklyearlybirds;

import java.util.Date;
import java.util.List;

import com.example.weeklyearlybirds.R;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

//import com.weekly.Earlybird.TwitMainActivity.GetUserTimelineThread;
//import com.weekly.Earlybird.TwitMainActivity.OAuthAccessTokenThread;
//import com.weekly.Earlybird.TwitMainActivity.RequestTokenThread;
//import com.weekly.Earlybird.TwitMainActivity.UpdateStatusThread;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 로딩 후 처음 화면 클래스입니다
 * @author 임은지
 * @since 2013.04.27.
 * @version 4.1
 */
public class FirstActivity extends Activity{
	public static final String TAG = "FirstActivity";

	/**
	 *<pre> 로딩 후 처음 화면 액티비티 작동 </pre>
	 *@param Bundle savedInstanceState 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);	
		
		Button listBtn = (Button) findViewById(R.id.bt_list);
		listBtn.setOnClickListener(new OnClickListener(){
			public void onClick (View v)
			{
				Intent intent=new Intent(FirstActivity.this,ArlistActivity.class);
				startActivity(intent);
				finish();
			}
				
			});	
		
		Button recordBtn = (Button) findViewById(R.id.bt_record);
		recordBtn.setOnClickListener(new OnClickListener(){
			public void onClick (View v)
			{
				Intent intent=new Intent(FirstActivity.this,GoogleMapActivity.class);
				startActivity(intent);
				finish();
			}
				
			});	
	}
		
	
	/**
	 *<pre> 핸드폰의 뒤로가기 버튼을 눌렀을 경우 시스템 종료시켜줌 </pre>
	 *@param int keyCode 키 코드값
	 *@param KeyEvent event 키 이벤트
	 *@return boolean
	 */
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	     switch (keyCode) {
	     case KeyEvent.KEYCODE_BACK:
	          finish();
	          System.exit(0);  
	     }
	     return super.onKeyDown(keyCode, event);
	 }	
}