package com.example.weeklyearlybirds;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

/**
 * 알람 설정 화면 클래스입니다
 * @author 임은지
 * @since 2013.05.04.
 * @version 3.2
 */
public class ArActivity_copy2 extends Activity {

	Intent intent;
	Button btnSave;

	EditText et_startdate;
	EditText et_meetTime;
	Calendar calendar = Calendar.getInstance();
	//TextView txtLabel;

	int HourOfDay;
	int Minute;
	int Year;
	int MonthOfYear;
	int DayOfMonth;
	
	int fcnt = 0;
	public static int cnt =1;

	DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			Year = year;
			MonthOfYear = monthOfYear;
			DayOfMonth = dayOfMonth;
			calendar.set(year, monthOfYear, dayOfMonth);
			setLabel();
		}
	};

	TimePickerDialog.OnTimeSetListener timeSetListenr = new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			HourOfDay = hourOfDay;
			Minute = minute;
			calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
			calendar.set(Calendar.MINUTE, minute);
			setLabel();
		}
	};

	/**
	 *<pre> 알람 추가할 때 정보 라벨 설정 </pre>
	 *@param
	 *@return boolean
	 */
	private void setLabel() {

		et_startdate.setText(new StringBuilder()
				.append(calendar.get(Calendar.YEAR)).append("-")
				.append(calendar.get(Calendar.MONTH) + 1).append("-")
				.append(calendar.get(Calendar.DAY_OF_MONTH)));
		et_meetTime.setText(new StringBuilder()
				.append(calendar.get(Calendar.HOUR_OF_DAY)).append(": ")
				.append(calendar.get(Calendar.MINUTE)));

	}

	// 네이버 길찾기 연결
	/**
	 *<pre> 네이버 길찾기 연결 </pre>
	 *@param Bundle savedInstanceState 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ar_copy2);

		//sd카드에 있는 텍스트 파일 개수 세기 (reqcode에 이용)
		String path = Environment.getExternalStorageDirectory()
				+ "/Android/data/com.example.weeklyearlybirds/testdata/";
		
		File sdFile = new File(path);
		//파일 총 갯수 얻어오기
		fcnt = sdFile.listFiles().length; 		
		

		Button exBtn = (Button) findViewById(R.id.btnIng);
		exBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent myl = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://map.naver.com/index.nhn?menu=route"));
				startActivity(myl);
			}

		});
		intent = getIntent();

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				// 저장버튼 누를 때 sdCard에 알람 정보 들어가게 함.
				// 해당 파일 이름으로 저장.
				String fileName = Environment.getExternalStorageDirectory()
						+ "/Android/data/com.example.weeklyearlybirds/testdata/"+Integer.toString(fcnt+1)+".txt";
				
				try {
					FileWriter fw = new FileWriter(fileName);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(et_meetTime.getText().toString());
					bw.close();
					fw.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
/*
				// 알람 리스트 activity 에 추가함.
				intent.putExtra("info", et_meetTime.getText().toString());
				setResult(RESULT_OK, intent);
								
				//두번째 요소 :requestCode. 여러개알람등록하려면 다른숫자
				PendingIntent sender = PendingIntent.getBroadcast(
						ArActivity_copy2.this, cnt, intent2, 0);
				
				//여러개 알람 울리게 하기위해 센더를 여러개 생성
				PendingIntent[] sender = new PendingIntent[cnt];
				*/
				
				Intent intent2 = new Intent(ArActivity_copy2.this,
						AlarmReceiver.class);
				
				PendingIntent sender = PendingIntent.getBroadcast(
						ArActivity_copy2.this, cnt, intent2, 0);
				
				//다른 알람을 울리게 하기 위한 변수 증가
				cnt++;

				AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
				am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
						sender);

				Toast.makeText(
						ArActivity_copy2.this,
						"시간설정:"
								+ Integer.toString(calendar
										.get(calendar.HOUR_OF_DAY))
								+ ":"
								+ Integer.toString(calendar
										.get(calendar.MINUTE)),
						Toast.LENGTH_LONG).show();

				// 알람 리스트 activity 에 추가함.
				Intent intent = new Intent(ArActivity_copy2.this,ArlistActivity.class);
								
				startActivity(intent);
				finish();

			}
		});

		Init();

		et_startdate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				new DatePickerDialog(ArActivity_copy2.this, dateSetListener,
						calendar.get(Calendar.YEAR), calendar
								.get(Calendar.MONTH), calendar
								.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

		et_meetTime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
/*
				InputMethodManager mgr = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			    mgr.hideSoftInputFromWindow(et_meetTime.getWindowToken(), 0);
				*/
				new TimePickerDialog(ArActivity_copy2.this, timeSetListenr,
						calendar.get(Calendar.HOUR_OF_DAY), calendar
								.get(Calendar.MINUTE), true).show();

			}
		});

		setLabel();
	}

	/**
	 *<pre> 액티비티 응답 받기 </pre>
	 *@param int requestCode 응답 요청 코드
	 *@param int resultCode 응답 결과 코드
	 *@param Intent data 인텐트 데이터
	 *@return void
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK)// 엑티비티가 정상적으로 종료되었을 경우
		{
			if (requestCode == 1) {

			}
		}

	}

	/**
	 *<pre> 알람정보 초기화 </pre>
	 *@param 
	 *@return void
	 */
	public void Init()

	{
		et_startdate = (EditText) findViewById(R.id.startDate);
		et_meetTime = (EditText) findViewById(R.id.meetTime);

	}

	/**
	 *<pre> 핸드폰의 뒤로가기 버튼을 눌렀을 경우 시스템 종료시켜줌 </pre>
	 *@param int keyCode 키 코드값
	 *@param KeyEvent event 키 이벤트
	 *@return boolean
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			Intent i = new Intent(this, ArlistActivity.class);
			startActivity(i);
			finish();

		}
		return super.onKeyDown(keyCode, event);
	}
}
