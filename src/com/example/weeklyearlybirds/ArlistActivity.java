package com.example.weeklyearlybirds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import com.example.weeklyearlybirds.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 알람 설정 화면 클래스입니다
 * @author 임은지
 * @since 2013.05.04.
 * @version 2.1
 */
public class ArlistActivity extends Activity {
	ListView listView;
	public static ArrayList<String> arrayList;
	ArrayAdapter<String> adapter;
	
	Intent intent;
	Button btnAdd;
	String alarmName;

	int fcnt = 0;

	/**
	 *<pre> 네이버 길찾기 연결 </pre>
	 *@param Bundle savedInstanceState 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ar_list);

		// sd카드에 있는 텍스트 파일 개수 세기 (reqcode에 이용)
		String path = Environment.getExternalStorageDirectory()
				+ "/Android/data/com.example.weeklyearlybirds/testdata/";
		File sdFile = new File(path);
		// 파일 총 갯수 얻어오기
		 fcnt = sdFile.listFiles().length;

		 //알람리스트 객체생성
		 arrayList = new ArrayList<String>();
		// 어댑터 객체 생성
			adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, arrayList);
			// 리스트뷰 객체 생성 & 어댑터 설정
			listView = (ListView) findViewById(R.id.listView);
			listView.setAdapter(adapter);
			listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		for(int i=0; i<fcnt; i++){
			
		String fileName = Environment.getExternalStorageDirectory()
				+ "/Android/data/com.example.weeklyearlybirds/testdata/"
				+ Integer.toString(i+1) + ".txt";

		// 파일을 읽어옴.
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);

			String readString = null;
			// readLine()으로 한줄씩 읽어온다.
			while ((readString = br.readLine()) != null) {
				// System.out.println(readString);
				alarmName = readString;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 알람리스트 객체생성 & 요소추가
		arrayList.add(alarmName);

		// 리스트뷰에 아이템 클릭 리스너 부착
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				// 아이템을 터치하면 토스트 띄움
				String str = (String) adapter.getItem(position);
				Toast.makeText(getBaseContext(), str, Toast.LENGTH_SHORT)
						.show();
			}
		});
		
		}

		btnAdd = (Button) findViewById(R.id.bt_add);

		/* 추가 버튼 */
		btnAdd.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				intent = new Intent(ArlistActivity.this, ArActivity_copy2.class);
				startActivityForResult(intent, 0);
				finish();

			}
		});

	}

	/*
	 * protected void onActivityResult(int requestCode, int resultCode, Intent
	 * data) { arrayList.add(data.getStringExtra("info").toString());
	 * adapter.notifyDataSetChanged(); super.onActivityResult(requestCode,
	 * resultCode, data); }
	 */

	/**
	 *<pre> 핸드폰의 뒤로가기 버튼을 눌렀을 경우 시스템 종료시켜줌 </pre>
	 *@param int keyCode 키 코드값
	 *@param KeyEvent event 키 이벤트
	 *@return boolean
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			Intent i = new Intent(this, FirstActivity.class);
			startActivity(i);
			finish();

		}
		return super.onKeyDown(keyCode, event);
	}

}