package com.example.weeklyearlybirds;

import com.example.weeklyearlybirds.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

/**
 * 로딩 화면 클래스입니다
 * @author 임은지
 * @since 2013.04.27.
 * @version 2.1
 */
public class IntroActivity extends Activity{
	Handler h;
	
	/**
	 *<pre> 로딩 후 처음 화면 액티비티 작동 </pre>
	 *@param Bundle savedInstanceState 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_intro);
		
		h = new Handler();
		h.postDelayed(irun,	3000);		
	}
	
	Runnable irun = new Runnable(){
		public void run(){
			Intent i=new Intent(IntroActivity.this,FirstActivity.class);
			startActivity(i);
			finish();
			overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
		}
	};

	/**
	 *<pre> 그 전 상태로 넘어가고 callback 제거함 </pre>
	 *@param
	 *@return void
	 */
	@Override
	public void onBackPressed(){
		super.onBackPressed();
		h.removeCallbacks(irun);
	}
}
