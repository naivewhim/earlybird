package com.example.weeklyearlybirds;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
 
/**
 * 리스트 보여주는 클래스입니다.
 * @author 안영학
 * @since 2013.05.04.
 * @version 2.1
 */
public class StatusListView extends ListView {
 
     // DataAdapter for this instance
     private StatusListAdapter adapter;
 
    // Listener for data selection
    private OnDataSelectionListener selectionListener;
 
    /**
	 *<pre> StatusListView 생성자 </pre>
	 *@param Context context 콘텍스트
	 *@return
	 */
    public StatusListView(Context context) {
        super(context);
 
        init();
    }
 
    /**
   	 *<pre> StatusListView 생성자 </pre>
   	 *@param Context context 콘텍스트
   	 *@param AttributeSet attrs 속성 설정
   	 *@return
   	 */
    public StatusListView(Context context, AttributeSet attrs) {
        super(context, attrs);
 
        init();
    }
 
    /**
	 *<pre> 초기 특성 설정 </pre>
	 *@param 
	 *@return void
	 */
    private void init() {
        setOnItemClickListener(new OnItemClickAdapter());
    }
 
    /**
	 *<pre> 데이터adapter 설정 </pre>
	 *@param BaseAdapter adapter 기본 어뎁터
	 *@return void
	 */
    public void setAdapter(BaseAdapter adapter) {
        super.setAdapter(adapter);
 
    }
 
    /**
   	 *<pre> 데이터adapter 받기 </pre>
   	 *@param
   	 *@return BaseAdapter
   	 */
    public BaseAdapter getAdapter() {
        return (BaseAdapter)super.getAdapter();
    }
 
    /**
     * set OnDataSelectionListener
     *
     * @param listener
     */
    /**
	 *<pre> 데이터 선택 리스너 설정 </pre>
	 *@param OnDataSelectionListener listener 데이터 선택 리스너
	 *@return void
	 */
    public void setOnDataSelectionListener(OnDataSelectionListener listener) {
        this.selectionListener = listener;
    }
 
    /**
     * get OnDataSelectionListener
     *
     * @return
     */
    /**
	 *<pre> 어플리케이션 첫 화면을 보여줌 </pre>
	 *@param 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
    public OnDataSelectionListener getOnDataSelectionListener() {
        return selectionListener;
    }
 
    /**
     * 아이템 선택 adapter
     * @author 안영학
     * @since 2013.05.04.
     * @version 2.1
     */
    class OnItemClickAdapter implements OnItemClickListener {
 
    	/**
    	 *<pre> OnItemClickAdapter 생성자 </pre>
    	 *@param
    	 *@return
    	 */
        public OnItemClickAdapter() {
        	
        }
 
        /**
    	 *<pre> 아이템 선택 </pre>
    	 *@param AdapterView parent 어뎁터뷰
    	 *@param View v 뷰
    	 *@param int position 위치 값
    	 *@param long id 아이디 값
    	 *@return void
    	 */
        public void onItemClick(AdapterView parent, View v, int position, long id) {
            if (selectionListener == null) {
                return;
            }
 
            // call the OnDataSelectionListener method
            selectionListener.onDataSelected(parent, v, position, id);
        }
    }
}