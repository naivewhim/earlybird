package com.example.weeklyearlybirds;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
 
/**
 * 처음 어플리케이션 실행시 화면 클래스입니다
 * @author 안영학
 * @since 2013.05.04.
 * @version 2.1
 */
public class StatusItemView extends LinearLayout {
 
    private ImageView mIcon;
 
    private TextView mText01;
 
    private TextView mText02;
 
    private TextView mText03;
 

	/**
	 *<pre> 아이템들을 보여줌 </pre>
	 *@param Context context 콘텍스트
	 *@return
	 */
    public StatusItemView(Context context) {
        super(context);
 
        // Layout Inflation
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listitem, this, true);
 
        // Set Icon
        mIcon = (ImageView) findViewById(R.id.iconItem);
 
        // Set Text 01
        mText01 = (TextView) findViewById(R.id.dataItem01);
 
        // Set Text 02
        mText02 = (TextView) findViewById(R.id.dataItem02);
 
        // Set Text 03
        mText03 = (TextView) findViewById(R.id.dataItem03);
 
    }
 
    /**
     * set Text
     *
     * @param index
     * @param data
     */
    

	/**
	 *<pre> 텍스트 설정 </pre>
	 *@param int index 인덱스 값
	 *@param String data 데이터 
	 *@return void
	 */
    public void setText(int index, String data) {
        if (index == 0) {
            mText01.setText(data);
        } else if (index == 1) {
            mText02.setText(data);
        } else if (index == 2) {
            mText03.setText(data);
        } else {
            throw new IllegalArgumentException();
        }
    }
 
	/**
	 *<pre> 아이콘 설정 </pre>
	 *@param Drawable icon 아이콘
	 *@return void
	 */
    public void setIcon(Drawable icon) {
        mIcon.setImageDrawable(icon);
    }
 

	/**
	 *<pre> 비트맵 설정 </pre>
	 *@param Bitmap bitmap 비트맵
	 *@return void
	 */
    public void setIcon(Bitmap bitmap) {
        mIcon.setImageBitmap(bitmap);
    }
 
}