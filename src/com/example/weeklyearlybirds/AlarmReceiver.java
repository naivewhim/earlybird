package com.example.weeklyearlybirds;

//import com.episode6.android.appalarm.pro.AalService;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * 알람이 울릴 때 정보 받는 클래스 입니다.
 * @author 임은지
 * @since 2013.05.04.
 * @version 3.2
 */
public class AlarmReceiver extends BroadcastReceiver {
	//알람사운드
	private static MySoundPlay mplay = null;	
	
	/**
	 *<pre> 알람정보를 받아 팝업을 뛰움 </pre>
	 *@param Context context 알람 콘텍스트
	 *@param Intent intent 알람 인텐트
	 *@return boolean 
	 */
	@Override
	public void onReceive(Context context, Intent intent) {

		Intent popupIntent = new Intent(context, Popup.class);
		popupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pi = PendingIntent.getActivity(context, 0, popupIntent,
				PendingIntent.FLAG_ONE_SHOT);
		
		try {
			pi.send();
		} catch (Exception e) {
			Toast.makeText(context, e.toString(), Toast.LENGTH_LONG);
		}
	}
}
