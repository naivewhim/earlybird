package com.example.weeklyearlybirds;


import java.util.Date;
import java.util.List;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 처음 어플리케이션 실행시 화면 클래스입니다
 * @author 임은지
 * @since 2013.05.27.
 * @version 4.1
 */
public class Popup extends Activity {

	TextView nameText;
    StatusListView statusList;
    StatusListAdapter statusAdapter;
 
    Button writeBtn;
    EditText writeInput;
    
	public static final String TAG = "MainActivity";    
    Button connectBtn;    
    Handler mHandler = new Handler();
	
	Button btnStop;
	
	// 알람사운드
	private static MediaPlayer mplay = null;

	/**
	 *<pre> 트위터 연결 액티비티 작동 </pre>
	 *@param Bundle savedInstanceState 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_popup);

		connectBtn = (Button) findViewById(R.id.connectBtn);
        connectBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                connect();
            }
        });
        
        nameText = (TextView) findViewById(R.id.nameText);
      
        statusList = (StatusListView) findViewById(R.id.statusList);
        statusAdapter = new StatusListAdapter(this, mHandler);
        statusList.setAdapter(statusAdapter);
        statusList.setOnDataSelectionListener(new OnDataSelectionListener() {
            public void onDataSelected(AdapterView parent, View v, int position, long id) {
                Status curItem = (Status) statusAdapter.getItem(position);
                String curText = curItem.getText();
 
                Toast.makeText(getApplicationContext(), "Selected : " + curText, 1000).show();
            }
        });
 
        writeBtn = (Button) findViewById(R.id.writeBtn);
        writeBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String statusText = writeInput.getText().toString();
                if (statusText.length() < 1) {
                    Toast.makeText(getApplicationContext(), "글을 입력하세요.", 1000).show();
                    return;
                }
 
                updateStatus(statusText);
            }
        });
 
        writeInput = (EditText) findViewById(R.id.writeInput);
        
		// 버튼 등록
		btnStop = (Button) findViewById(R.id.btnSave);
		PowerManager pm = (PowerManager) this
				.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP, "AppAlarmReceiver");
		wl.acquire();

		Toast.makeText(this, "alarm", Toast.LENGTH_LONG).show();

		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		try {
			if (mplay == null) {
				mplay = MediaPlayer.create(getBaseContext(), R.raw.happythings);
				mplay.start();
				mplay.setLooping(true);
			}

		} catch (Exception e) {
			Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
		}

		// 알람 종료버튼
		btnStop = (Button) findViewById(R.id.bt_stop);
		btnStop.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				mplay.release();
			}
		});
	}

	/**
	 *<pre> UpdateStatusThread 시작 </pre>
	 *@param String statusText 입력한 텍스트
	 *@return void
	 */
	private void updateStatus(String statusText) {
        
        UpdateStatusThread thread = new UpdateStatusThread(statusText);
        thread.start();   
    }
	
	/**
	 * 트위터 타임라인 업데이트 스레드 클래스입니다.
	 * @author 안영학
	 * @since 2013.05.04.
	 * @version 2.1
	 */
	class UpdateStatusThread extends Thread {
       
		String statusText;
         
        /**
    	 *<pre> 어플리케이션 첫 화면을 보여줌 </pre>
    	 *@param 값을 유지하여 복원이 가능하게 하는 객체
    	 *@return void
    	 */
        public UpdateStatusThread(String inText) {
            statusText = inText;
        }
         
        /**
    	 *<pre> 트위터 타임라인 글 업데이트 실행 </pre>
    	 *@param
    	 *@return void
    	 */
        public void run() {
            try {
                Status status = BasicInfo.TwitInstance.updateStatus(statusText);
                final Date curDate = status.getCreatedAt();
 
                mHandler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "글을 업데이트했습니다 : " + BasicInfo.DateFormat.format(curDate), Toast.LENGTH_SHORT).show();
 
                        showUserTimeline();
                    }
                });
                 
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

	/**
	 *<pre> 트위터 연결 시도 </pre>
	 *@param 
	 *@return void
	 */
	private void connect() {
        Log.d(TAG, "connect() called.");
 
        if (BasicInfo.TwitLogin) {
            Log.d(TAG, "twitter already logged in.");
            Toast.makeText(getBaseContext(), "twitter already logged in.", Toast.LENGTH_LONG).show();
 
            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
 
                builder.setOAuthAccessToken(BasicInfo.TWIT_KEY_TOKEN);
                builder.setOAuthAccessTokenSecret(BasicInfo.TWIT_KEY_TOKEN_SECRET);
                builder.setOAuthConsumerKey(BasicInfo.TWIT_CONSUMER_KEY);
                builder.setOAuthConsumerSecret(BasicInfo.TWIT_CONSUMER_SECRET);
 
                Configuration config = builder.build();
                TwitterFactory tFactory = new TwitterFactory(config);
                BasicInfo.TwitInstance = tFactory.getInstance();
 
                Toast.makeText(getBaseContext(), "twitter connected.", Toast.LENGTH_LONG).show();
 
            } catch (Exception ex) {
                ex.printStackTrace();
            }
 
            showUserTimeline();
 
        } else {
             
            RequestTokenThread thread = new RequestTokenThread();
            thread.start();     
        }
    }
    
	/**
	 * RequestToken 요청 스레드 클래스 입니다.
	 * @author 안영학
	 * @since 2013.05.04.
	 * @version 2.1
	 */
    class RequestTokenThread extends Thread {
    	
        public void run() {
 
            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setDebugEnabled(true);
                builder.setOAuthConsumerKey(BasicInfo.TWIT_CONSUMER_KEY);
                builder.setOAuthConsumerSecret(BasicInfo.TWIT_CONSUMER_SECRET);
 
                TwitterFactory factory = new TwitterFactory(builder.build());
                Twitter mTwit = factory.getInstance();
                final RequestToken mRequestToken = mTwit.getOAuthRequestToken();
                String outToken = mRequestToken.getToken();
                String outTokenSecret = mRequestToken.getTokenSecret();
 
                Log.d(TAG, "Request Token : " + outToken + ", " + outTokenSecret);
                Log.d(TAG, "AuthorizationURL : " + mRequestToken.getAuthorizationURL());
 
                BasicInfo.TwitInstance = mTwit;
                BasicInfo.TwitRequestToken = mRequestToken;
        
                mHandler.post(new Runnable() {
                    public void run() {
 
                        Intent intent = new Intent(getApplicationContext(), TwitLogin.class);
                        intent.putExtra("authUrl", mRequestToken.getAuthorizationURL());
                        startActivityForResult(intent, BasicInfo.REQ_CODE_TWIT_LOGIN);
                    }
                });
                 
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
     
    /**
	 *<pre> 액티비티 응답 받기 </pre>
	 *@param int requestCode 응답 요청 코드
	 *@param int resultCode 응답 결과 코드
	 *@param Intent data 인텐트 데이터
	 *@return void
	 */
    protected void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
 
        if (resultCode == RESULT_OK) {
            if (requestCode == BasicInfo.REQ_CODE_TWIT_LOGIN) {
                 
                OAuthAccessTokenThread thread = new OAuthAccessTokenThread(resultIntent);
                thread.start();  
            }
        }
    }
    
    /**
     * 처음 OAuth토큰 인증 스레드 클래스입니다.
     * @author 안영학
     * @since 2013.05.04.
     * @version 2.1
     */
    class OAuthAccessTokenThread extends Thread {
    	
        Intent resultIntent;
         
        /**
    	 *<pre> OAuthAccessTokenThread 생성자 </pre>
    	 *@param Intent intent 인텐트
    	 *@return
    	 */
        public OAuthAccessTokenThread(Intent intent) {
            resultIntent = intent;
        }
         
        /**
    	 *<pre> 트위터 연동 실행 </pre>
    	 *@param 
    	 *@return void
    	 */
        public void run() {
            try {
                Twitter mTwit = BasicInfo.TwitInstance;
 
                AccessToken mAccessToken = mTwit.getOAuthAccessToken(BasicInfo.TwitRequestToken, resultIntent.getStringExtra("oauthVerifier"));
 
                BasicInfo.TwitLogin = true;
                BasicInfo.TWIT_KEY_TOKEN = mAccessToken.getToken();
                BasicInfo.TWIT_KEY_TOKEN_SECRET = mAccessToken.getTokenSecret();
 
                BasicInfo.TwitAccessToken = mAccessToken;
 
                BasicInfo.TwitScreenName = mTwit.getScreenName();
 
                mHandler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(getBaseContext(), "Twitter connection succeeded : " + BasicInfo.TWIT_KEY_TOKEN, Toast.LENGTH_LONG).show();
     
                        showUserTimeline();
                    }
                });
 
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
 
    /**
	 *<pre> 트위터 타임라인 보여줌 </pre>
	 *@param 
	 *@return void
	 */
    private void showUserTimeline() {
    	
        Log.d(TAG, "showUserTimeline() called.");
 
        connectBtn.setVisibility(View.GONE);
        nameText.setVisibility(View.VISIBLE);
        nameText.setText(BasicInfo.TwitScreenName);
 
        // UserTimeline 요청
        GetUserTimelineThread thread = new GetUserTimelineThread();
        thread.start();
    }
 
    /**
	 * 트위터 타임라인 요청하는 스레드 클래스입니다.
	 * @author 안영학
	 * @since 2013.05.04.
	 * @version 2.1
	 */
    class GetUserTimelineThread extends Thread {
        public void run() {
            getUserTimeline();
        }
         
        /**
    	 *<pre> 트위터 타임라인 가져와서 보여주기 </pre>
    	 *@param 
    	 *@return void
    	 */
        private void getUserTimeline() {
            Twitter mTwit = BasicInfo.TwitInstance;
 
            try {
                final List<Status> statuses = mTwit.getUserTimeline();
 
                mHandler.post(new Runnable() {
                    public void run() {
                        statusAdapter.setListItems(statuses);
                        statusAdapter.notifyDataSetChanged();
                    }
                });
                 
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }
   
    /**
	 *<pre> 실행 중지 및 속성 저장 </pre>
	 *@param 
	 *@return void
	 */
    protected void onPause() {
    	
        super.onPause();
 
        saveProperties();
    }
 
    /**
	 *<pre> 특성 재설정 </pre>
	 *@param 
	 *@return void
	 */
    protected void onResume() {
    	
        super.onResume();
 
        loadProperties();
    }
 
    /**
	 *<pre> 특성들 저장 </pre>
	 *@param 
	 *@return void
	 */
    private void saveProperties() {
    	
        SharedPreferences pref = getSharedPreferences("TWIT", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
 
        editor.putBoolean("TwitLogin", BasicInfo.TwitLogin);
        editor.putString("TWIT_KEY_TOKEN", BasicInfo.TWIT_KEY_TOKEN);
        editor.putString("TWIT_KEY_TOKEN_SECRET", BasicInfo.TWIT_KEY_TOKEN_SECRET);
 
        editor.commit();
    }
 
    /**
	 *<pre> 특성들 로드 </pre>
	 *@param 
	 *@return void
	 */
    private void loadProperties() {
        
    	SharedPreferences pref = getSharedPreferences("TWIT", MODE_PRIVATE);
 
        BasicInfo.TwitLogin = pref.getBoolean("TwitLogin", false);
        BasicInfo.TWIT_KEY_TOKEN = pref.getString("TWIT_KEY_TOKEN", "");
        BasicInfo.TWIT_KEY_TOKEN_SECRET = pref.getString("TWIT_KEY_TOKEN_SECRET", "");
    }
     
    /**
	 *<pre> 메뉴 생성 </pre>
	 *@param Menu menu 메뉴
	 *@return boolean
	 */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    /**
	 *<pre> 핸드폰의 뒤로가기 버튼을 눌렀을 경우 시스템 종료시켜줌 </pre>
	 *@param int keyCode 키 코드값
	 *@param KeyEvent event 키 이벤트
	 *@return boolean
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			Intent i = new Intent(this, ArlistActivity.class);
			startActivity(i);
			mplay.release();
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
