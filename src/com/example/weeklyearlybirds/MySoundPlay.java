package com.example.weeklyearlybirds;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * 알람 소리 설정 클래스입니다.
 * @author 임은지
 * @since 2013.05.11
 * @version 4.1
 */
public class MySoundPlay {
	MediaPlayer mp = null;    	
	
	/**
	 *<pre> MySoundPlay 생성자 </pre>
	 *@param Context context 
	 *@param int id
	 *@return
	 */
	public MySoundPlay ( Context context, int id ) {
		mp = MediaPlayer.create(context, id);
	}    
	
	/**
	 *<pre> 알람음 시작 </pre>
	 *@param 
	 *@return void
	 */
	public void play() {
		mp.seekTo(0);
		mp.start();
	}
}
