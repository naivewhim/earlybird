package com.example.weeklyearlybirds;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * 트위터 로그인 클래스입니다.
 * @author 안영학
 * @since 2013.05.04.
 * @version 4.1
 */
public class TwitLogin extends Activity {
	public static final String TAG = "TwitLogin";
	
	/**
	 *<pre> 트위터 로그인 인증 </pre>
	 *@param Bundle savedInstanceState 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twit_login);
 
        WebView webview = (WebView) findViewById(R.id.webView);
        webview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d(TAG, ".onPageFinished");
 
                if (url != null && url.equals("http://mobile.twitter.com/")) {
                    finish();
                    
                    Log.d(TAG, "if");
                    
                } else if (url != null && url.startsWith(BasicInfo.TWIT_CALLBACK_URL)) {
                	
                	Log.d(TAG, "else if");
                	
                    String[] params = url.split("\\?")[1].split("&");
                    String oauthToken = "";
                    String oauthVerifier = "";
 
                    try {
                        if (params[0].startsWith("oauth_token")) {
                            oauthToken = params[0].split("=")[1];
                        } else if (params[1].startsWith("oauth_token")) {
                            oauthToken = params[1].split("=")[1];
                        }
 
                        if (params[0].startsWith("oauth_verifier")) {
                            oauthVerifier = params[0].split("=")[1];
                        } else if (params[1].startsWith("oauth_verifier")) {
                            oauthVerifier = params[1].split("=")[1];
                        }
 
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("oauthToken", oauthToken);
                        resultIntent.putExtra("oauthVerifier", oauthVerifier);
 
                        Log.d(TAG, "resultIntent");
                        
                        setResult(RESULT_OK, resultIntent);
                        finish();
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
 
 
        Intent passedIntent = getIntent();
        String authUrl = passedIntent.getStringExtra("authUrl");
        webview.loadUrl(authUrl);
        Log.d(TAG, "webView");
 
    }
 
}