package com.example.weeklyearlybirds;

 
import java.text.SimpleDateFormat;
 
import twitter4j.Twitter;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 * 트위터 기본 키 값 정보들을 담고 있는 클래스입니다.
 * @author 안영학
 * @since 2013.05.04.
 * @version 2.1
 */
public class BasicInfo {
 
	//API 키값 = CONSUMER KEY 값
    public static final String TWIT_API_KEY = "p50CN35DqM5t7qOEBPRNg";
    public static final String TWIT_CONSUMER_KEY = "p50CN35DqM5t7qOEBPRNg";
    public static final String TWIT_CONSUMER_SECRET = "Z45Kw2Gz8QsuCOuXTzcUFw1yc8y8tjQmPw8El7Js";
    public static final String TWIT_CALLBACK_URL = "http://android-town.org";
 
    public static final int REQ_CODE_TWIT_LOGIN = 1001;
 
    public static boolean TwitLogin = false;
    public static Twitter TwitInstance = null;
    public static AccessToken TwitAccessToken = null;
    public static RequestToken TwitRequestToken = null;
 
    public static String TWIT_KEY_TOKEN = "";
    public static String TWIT_KEY_TOKEN_SECRET = "";
    public static String TwitScreenName = "";
 
    public static SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분");
 
}