package com.example.weeklyearlybirds;

import java.net.URL;
import java.util.Date;
import java.util.List;
 
import twitter4j.Status;
import twitter4j.User;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
 
/**
 * 리스트를 위한 adapter 클래스
 * @author 안영학
 * @since 2013.05.04.
 * @version 2.1
 */
public class StatusListAdapter extends BaseAdapter {
 
    public static String TAG = "StatusListAdapter";
 
    private Context mContext;
 
    private List<Status> mStatuses = null;
 
    Handler mHandler;
     
	/**
	 *<pre> StatusListAdapter 생성자 </pre>
	 *@param Context context
	 *@param Handler handler
	 *@return void
	 */
    public StatusListAdapter(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
    }
 
	/**
	 *<pre> 리스트 아이템 설정 </pre>
	 *@param List<Status> list 리스트들
	 *@return void
	 */
    public void setListItems(List<Status> list) {
        mStatuses = list;
    }
 
	/**
	 *<pre> 숫자 세기 </pre>
	 *@param
	 *@return int
	 */
    public int getCount() {
        if (mStatuses == null) {
            return 0;
        } else {
            return mStatuses.size();
        }
    }
 
	/**
	 *<pre> 아이템 가져오기 </pre>
	 *@param int position 위치 값 
	 *@return Object
	 */
    public Object getItem(int position) {
        if (mStatuses == null) {
            return null;
        } else {
            return mStatuses.get(position);
        }
    }
 
	/**
	 *<pre> 모든 아이템 선택 여부 </pre>
	 *@param 
	 *@return boolean
	 */
    public boolean areAllItemsSelectable() {
        return false;
    }
 
    /**
	 *<pre> 테이블 선택 여부 </pre>
	 *@param int position 위치 값 
	 *@return boolean
	 */
    public boolean isSelectable(int position) {
        return true;
    }
 
	/**
	 *<pre> 아이템 아이디 얻어오기 </pre>
	 *@param int position 위치 값 
	 *@return long
	 */
    public long getItemId(int position) {
        return position;
    }
 
	/**
	 *<pre> 뷰 얻어오기 </pre>
	 *@param int position 위치 값
	 *@param View convertView 뷰 종류
	 *@param ViewGroup parent 뷰 그룹
	 *@return View
	 */
    public View getView(int position, View convertView, ViewGroup parent) {
        StatusItemView itemView;
        if (convertView == null) {
            itemView = new StatusItemView(mContext);
        } else {
            itemView = (StatusItemView) convertView;
        }
 
        try {
            Status curStatus = mStatuses.get(position);
 
            User user = curStatus.getUser();
            String userName = user.getName();
            String userScreenName = user.getScreenName();
            URL url = new URL(user.getProfileImageURL());
 
            Date date = curStatus.getCreatedAt();
            String data = curStatus.getText();
 
            itemView.setText(0, userScreenName);
 
            String dateStr = BasicInfo.DateFormat.format(date);
            itemView.setText(1, dateStr);
 
            itemView.setText(2, data);
 
            if (url != null) {
                Log.d(TAG, "Bitmap URL : " + url);
                 
                GetBitmapThread thread = new GetBitmapThread(itemView, url);
                thread.start();
                 
            }
 
        } catch(Exception ex) {
            ex.printStackTrace();
        }
 
        return itemView;
    }
 
    /**
     * 비트맵 얻어오는 스레드 클래스입니다.
     * @author 안영학
     * @since 2013.05.04.
     * @version 2.1
     */
    class GetBitmapThread extends Thread {
        StatusItemView itemView;
        URL url;
         
    	/**
    	 *<pre> GetBitmapThread 생성자 </pre>
    	 *@param StatusItemView view 뷰 종류
    	 *@param URL inUrl 유알엘 값
    	 *@return void
    	 */
        public GetBitmapThread(StatusItemView view, URL inUrl) {
            itemView = view;
            url = inUrl;
        }
         
    	/**
    	 *<pre> 아이콘 설정 실행 </pre>
    	 *@param 
    	 *@return void
    	 */
        public void run() {
            try {
            final Bitmap curBitmap = BitmapFactory.decodeStream(url.openStream());
                 
                mHandler.post(new Runnable() {
                    public void run() {
                        itemView.setIcon(curBitmap);
                    }
                });
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    } 
}