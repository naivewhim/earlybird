package com.example.weeklyearlybirds;

import android.view.View;
import android.widget.AdapterView;

/**
 * 아이템이 선택되었을 떄
 * @author 안영학
 * @since 2013.05.04.
 * @version 2.1
 */
public interface OnDataSelectionListener {
 
	/**
	 *<pre> 데이터 선택시 </pre>
	 *@param AdapterView parent
	 *@param View v
	 *@param int position
	 *@param long id
	 *@return void
	 */
    public void onDataSelected(AdapterView parent, View v, int position, long id);
}