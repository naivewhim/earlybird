package com.example.weeklyearlybirds;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * 처음 어플리케이션 실행시 화면 클래스입니다
 * @author 임은지
 * @since 2013.04.27.
 * @version 4.2
 */
public class GoogleMapActivity extends FragmentActivity implements
		LocationListener {

	private GoogleMap mmap;
	private LocationManager locationManager;
	private String provider;

	/**
	 *<pre> GoogleMapActivity 생성자 </pre>
	 *@param 
	 *@return
	 */
	public GoogleMapActivity() {
	}

	/**
	 *<pre> 구글맵을 위한 위치정보 설정 </pre>
	 *@param Bundle savedInstanceState 값을 유지하여 복원이 가능하게 하는 객체
	 *@return void
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_map);
	
		GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, true);

		if (provider == null) { // 위치정보 설정이 안되어 있으면 설정하는 엑티비티로 이동.
			new AlertDialog.Builder(this)
					.setTitle("위치서비스 동의")
					.setNeutralButton("이동",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									startActivityForResult(
											new Intent(
													android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
											0);
								}
							})
					.setOnCancelListener(
							new DialogInterface.OnCancelListener() {
								@Override
								public void onCancel(DialogInterface dialog) {
									finish();
								}
							}).show();
		} else { // 위치 정보 설정이 되어 있으면 현재위치를 받아옴.
			locationManager.requestLocationUpdates(provider, 1, 1, this);
			setUpMapIfNeeded();
		}
	}

	/**
	 *<pre> 액티비티 응답 받기 </pre>
	 *@param int requestCode 응답 요청 코드
	 *@param int resultCode 응답 결과 코드
	 *@param Intent data 인텐트 데이터
	 *@return void
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {// 위치설정
																					// 엑티비티
																					// 종료
																					// 후
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 0:
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			provider = locationManager.getBestProvider(criteria, true);
			if (provider == null) {// 사용자가 위치설정동의 안했을때 종료
				finish();
			} else {// 사용자가 위치설정 동의 했을때
				locationManager.requestLocationUpdates(provider, 1L, 2F, this);
				setUpMapIfNeeded();
			}
			break;
		}
	}

	/**
	 *<pre> 그 전 상태로 넘어가고 액티비티 종료시켜줌 </pre>
	 *@param
	 *@return void
	 */
	@Override
	public void onBackPressed() {
		this.finish();
	}

	/**
	 *<pre> 구글맵 재설정 </pre>
	 *@param 
	 *@return void
	 */
	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();

	}

	/**
	 *<pre> 구글맵 중지 </pre>
	 *@param 
	 *@return void
	 */
	@Override
	protected void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}

	/**
	 *<pre> 구글맵 변경이 필요한 경우 </pre>
	 *@param 
	 *@return void
	 */
	private void setUpMapIfNeeded() {
		if (mmap == null) {
			mmap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			if (mmap != null) {
				setUpMap();
			}
		}
	}

	/**
	 *<pre> 구글맵 설정 </pre>
	 *@param 
	 *@return void
	 */
	private void setUpMap() {
		mmap.setMyLocationEnabled(true);
		mmap.getMyLocation();

	}

	boolean locationTag = true;

	/**
	 *<pre> 위치정보 변경시 </pre>
	 *@param Location location 위치정보
	 *@return void
	 */
	@Override
	public void onLocationChanged(Location location) {
		if (locationTag) {// 한번만 위치를 가져오기 위해서 tag를 줌.
			Log.d("myLog", "onLocationChanged: !!" + "onLocationChanged!!");
			double lat = location.getLatitude();
			double lng = location.getLongitude();

			//현재 위치 토스트 메세지로 띄우기.
			Toast.makeText(this, "위도  : " + lat + " 경도: " + lng,
					Toast.LENGTH_SHORT).show();
			locationTag = false;
		}

	}

	/**
	 *<pre> onProviderDisabled 생성자 </pre>
	 *@param String provider
	 *@return boolean
	 */
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	/**
	 *<pre> onProviderEnabled 생성자 </pre>
	 *@param String provider
	 *@return void
	 */
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	/**
	 *<pre> onStatusChanged 생성자 </pre>
	 *@param String provider
	 *@param int status
	 *@param Bundle extras
	 *@return void
	 */
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	/**
	 *<pre> 핸드폰의 뒤로가기 버튼을 눌렀을 경우 시스템 종료시켜줌 </pre>
	 *@param int keyCode 키 코드값
	 *@param KeyEvent event 키 이벤트
	 *@return boolean
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			Intent i = new Intent(GoogleMapActivity.this, FirstActivity.class);
			startActivity(i);
			finish();
			return true;

		}
		return super.onKeyDown(keyCode, event);
	}

}
